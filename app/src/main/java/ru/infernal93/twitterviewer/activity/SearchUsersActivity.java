package ru.infernal93.twitterviewer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Arrays;
import java.util.Collection;
import ru.infernal93.twitterviewer.R;
import ru.infernal93.twitterviewer.adapter.UsersAdapter;
import ru.infernal93.twitterviewer.pojo.User;

public class SearchUsersActivity extends AppCompatActivity {

    private RecyclerView userRecyclerView;
    private UsersAdapter usersAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_users);

        initRecyclerView();
        searchUsers();
    }

    private void initRecyclerView() {
        userRecyclerView = findViewById(R.id.users_recyclerView);
        userRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        UsersAdapter.OnUsersClickListener onUsersClickListener = new UsersAdapter.OnUsersClickListener() {
            @Override
            public void onUserClick(User user) {
                Intent intent = new Intent(SearchUsersActivity.this, UserInfoActivity.class);
                intent.putExtra(UserInfoActivity.USER_ID, user.getId());
                startActivity(intent);
                //Toast.makeText(SearchUsersActivity.this, "user" + user.getName(), Toast.LENGTH_LONG).show();
            }
        };

        usersAdapter = new UsersAdapter(onUsersClickListener);
        userRecyclerView.setAdapter(usersAdapter);

    }



    private void searchUsers() {
        Collection<User> users = getUsers();
        usersAdapter.setItems(users);
    }

    private Collection<User> getUsers() {
        return Arrays.asList(new User(
                        929257819349700608L,
                        "https://sun1-89.userapi.com/c854428/v854428617/db24a/tCZC6Sidjz4.jpg",
                        "Карина",
                        "kara_design12",
                        "Iam very good designer",
                        "Russia, Ivanovo",
                        25,
                        63
                ),
                new User(
                        44196397L,
                        "https://sun9-35.userapi.com/c857728/v857728141/400b4/JBKer8_m1zQ.jpg",
                        "Алан",
                        "alan_ray96",
                        "Iam very good architector",
                        "Russia, YFA",
                        93,
                        79

                ));
    }
}
