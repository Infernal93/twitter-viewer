package ru.infernal93.twitterviewer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.Arrays;
import java.util.Collection;
import ru.infernal93.twitterviewer.R;
import ru.infernal93.twitterviewer.adapter.TweetAdapter;
import ru.infernal93.twitterviewer.pojo.Tweet;
import ru.infernal93.twitterviewer.pojo.User;

public class UserInfoActivity extends AppCompatActivity {

    public static final String USER_ID = "userId";

    private ImageView userImageView;
    private TextView nameTextView;
    private TextView nickTextView;
    private TextView descriptionTextView;
    private TextView locationTextView;
    private TextView followingCountTextView;
    private TextView followersCountTextView;

    private RecyclerView tweetRecyclerView;
    private TweetAdapter  tweetAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        userImageView = findViewById(R.id.user_imageView);
        nameTextView = findViewById(R.id.userName_textView);
        nickTextView = findViewById(R.id.userNickName_textView);
        descriptionTextView = findViewById(R.id.userDescription_textView);
        locationTextView = findViewById(R.id.userLocation_textView);
        followingCountTextView = findViewById(R.id.userFollowing_count_textView);
        followersCountTextView = findViewById(R.id.userFollowers_count_textView);


        // set Text in UserInfo
        loadUserInfo();

        initRecyclerView();
        loadTweets();
    }

    private Collection<Tweet> getTweets() {
        return Arrays.asList(
                new Tweet(getUser(), 1L, "Thu Dec 13 07:31:08 +0000 217","Very long description",
                        41L, 45L, "https://sun9-29.userapi.com/c831209/v831209102/149564/m_54uYzygb0.jpg"),
                new Tweet(getUser(), 1L, "Thu Dec 13 07:31:08 +0000 217","Very long description",
                        92L, 19L, "https://sun9-7.userapi.com/c638518/v638518500/2c53e/pPSPIUkyt54.jpg"),
                new Tweet(getUser(), 1L, "Thu Dec 13 07:31:08 +0000 217","Very long description",
                        69L, 53L, "https://sun9-43.userapi.com/c857428/v857428272/23ba3/zLWkxl50Plc.jpg")
        );
    }

    private void loadTweets() {
        Collection<Tweet> tweets = getTweets();
        tweetAdapter.setItems(tweets);
    }

    private void initRecyclerView() {
        tweetRecyclerView = findViewById(R.id.tweet_recyclerView);
        tweetRecyclerView.setLayoutManager( new LinearLayoutManager(this));

        tweetAdapter = new TweetAdapter();
        tweetRecyclerView.setAdapter(tweetAdapter);
    }

    private void displayUserInfo(User user) {
        Picasso.with(this).load(user.getImageUrl()).into(userImageView);
        nameTextView.setText(user.getName());
        nickTextView.setText(user.getNick());
        descriptionTextView.setText(user.getDescription());
        locationTextView.setText(user.getLocation());
        String followingCount = String.valueOf(user.getFollowingCount());
        followingCountTextView.setText(followingCount);
        String followersCount = String.valueOf(user.getFollowersCount());
        followersCountTextView.setText(followersCount);
    }

    private void loadUserInfo() {
        User user = getUser();
        displayUserInfo(user);
    }

    private User getUser(){
        return new User(1L, "http://i.imgur.com/DvpvklR.png", "Armen Mkhitaryan", "armen_93",
                "Sample Text in Description", "Moscow, Russia", 42, 42);
    }
}
