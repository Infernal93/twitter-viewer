package ru.infernal93.twitterviewer.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import ru.infernal93.twitterviewer.R;
import ru.infernal93.twitterviewer.pojo.User;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersViewHolder> {

    private List<User> userList = new ArrayList<>();
    private OnUsersClickListener onUsersClickListener;

    public UsersAdapter(OnUsersClickListener onUsersClickListener) {
        this.onUsersClickListener = onUsersClickListener;
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_view, parent, false);
        return new UsersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder holder, int position) {
        holder.bind(userList.get(position));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void setItems(Collection<User> user) {
        userList.addAll(user);
        notifyDataSetChanged();
    }

    public void clearItems() {
        userList.clear();
        notifyDataSetChanged();
    }

    class UsersViewHolder extends RecyclerView.ViewHolder {

        private ImageView userImageView;
        private TextView nameTextView;
        private TextView nickTextView;


        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
            userImageView = itemView.findViewById(R.id.profile_imageView);
            nameTextView = itemView.findViewById(R.id.user_name_TextView);
            nickTextView = itemView.findViewById(R.id.user_nick_TextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = userList.get(getLayoutPosition());
                    onUsersClickListener.onUserClick(user);
                }
            });
        }

        public void bind(User user) {
            nameTextView.setText(user.getName());
            nickTextView.setText(user.getNick());
            Picasso.with(itemView.getContext()).load(user.getImageUrl()).into(userImageView);
        }
    }

    public interface OnUsersClickListener {
        void onUserClick(User user);
    }
}
